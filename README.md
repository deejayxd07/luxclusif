## 📋Install

### Requirements

- [Node](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)

### Installation Steps

1. Clone the repo
2. Install dependencies by running `yarn` inside the project folder

`

## Running the app

`yarn start`

## Accessing the app

After `yarn start` you may access the app on `http://localhost:3000`.

But if you're not feeling on cloning and running the app locally, I have deployed the app on netlify, you may access it on https://tender-kowalevski-e16bf2.netlify.app/ (please excuse the name, netlify gives weired free domain names)

## Running the tests

`yarn test`

# Running typescript checks

`yarn tsc`

## Generating a production build

`yarn build`

Builds the app for production to the build folder.

## Credentials

Email: pleasehiredavid@luxclusif.com

Password: ShouldWe?PleaseDo:)

## 📂 Project Structure

```
Root
├── .domakey/           (code generator)
│   ├── component.js
│   └── route.js
├── public/             (static files)
├── src/                (project source files)
│   ├── components/     (resuable components)
│       ├── core
│       ├── common
│   ├── index.ts
│   └── routes          (contains all the pages / routes)
│       ├── index.tsx   (routes setup)
|       └── ... routes
│
└── package.json
```

## Components

**core/** - core UI components e.g. AuthGuard

**common/** - components specific to a feature that can be reused on other places e.g. FormikInput

**\*.container.tsx** - Container components: concerned with how things work, components that holds the logic

**\*.view.tsx** - Presentational / dumb components: Components that holds the markup, put less logic as possible.

## Code-generation

The project comes with code generation, to speed things up. We use domakey for this.

**Routes**

`yarn codegen route NewRoute`

This will create

```
├── routes
|   ├── NewRoute (New)
    |   ├── NewRoute.container.tsx
    |   ├── NewRoute.props.ts
    |   ├── NewRoute.style.ts
    |   ├── NewRoute.view.tsx
    |   └── index.tsx
└── ... other routes
```

also import the route and add an entry in `ROUTES` object of `/src/routes/index.tsx`

**Components**

`yarn codegen component NewComponent`

This will create

```
└── components
    └── <component category>
        └── NewComponent (New)
        ├── NewComponent.container.tsx (optional)
        ├── NewComponent.props.ts
        ├── NewComponent.style.ts
        ├── NewComponent.view.tsx
        └── index.tsx
```

## State Management and API (Data Persistence)

We use [Zustand](https://github.com/pmndrs/zustand) with [Immer](https://github.com/immerjs/immer) for managing our states

For the purposes of this exam, I decided to use zustand to handle the "API" side, values are persisted on local storage thru zustand persist middleware.

## Form State

We use [Formik](https://formik.org/) for handling our form state. It's pretty cool, makes life easier imho

## Translations

We use [react.i18next](https://react.i18next.com/) for the translations
The translations defaults to en.
At the moment it is very bare, we only have en and fr which was not translated since I don't know french (google might give strange answers)
