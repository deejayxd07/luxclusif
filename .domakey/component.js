const containerBody = `import React from 'react';

import {{ComponentName}}View from './{{ComponentName}}.view';
import { {{ComponentName}}Props } from './{{ComponentName}}.props';

const {{ComponentName}}Container = (props: {{ComponentName}}Props): JSX.Element => {
  return <{{ComponentName}}View />;
};

export default {{ComponentName}}Container;
`;

const styleBody = `import styled from '@emotion/styled';

export const Container = styled.div\`\`;
`;

const smartPropsBody = `export type {{ComponentName}}GeneratedProps = {};

export type {{ComponentName}}Props = {};
`;

const dumbPropsBody = `export type {{ComponentName}}Props = {};
`;

const smartViewBody = `import React from 'react';
import { Container } from './{{ComponentName}}.style';
import { {{ComponentName}}GeneratedProps } from './{{ComponentName}}.props';

const {{ComponentName}}View = (props: {{ComponentName}}GeneratedProps): JSX.Element => {
  return <Container />;
};

export default {{ComponentName}}View;
`;

const dumbViewBody = `import React from 'react';
import { Container } from './{{ComponentName}}.style';
import { {{ComponentName}}Props } from './{{ComponentName}}.props';

const {{ComponentName}}View = (props: {{ComponentName}}Props): JSX.Element => {
  return <Container />;
};

export default {{ComponentName}}View;
`;

const smartIndexBody = `export { default } from './{{ComponentName}}.container';
`;

const dumbIndexBody = `export { default } from './{{ComponentName}}.view';
`;

const help = `Adds a new React component.

This will create the new component folder (/src/components/<component type>/MyComponentName) and the component files.

\`npm run domakey component MyComponentName\`

Options:
 --y            Accept all default suggestions
`;

module.exports = async ({ cliArgs, cliFlags, templateName, makey }) => {
  if (cliFlags['h']) {
    makey.print(help);
    return;
  }

  const componentName = makey.toLowerCaseFirst(
    cliArgs[0] || (await makey.ask('Name of your component:')),
  );
  if (!componentName) throw Error('Please provide a component name');
  const compTypeRaw = makey.toLowerCaseFirst(
    cliArgs[1] ||
      (await makey.ask(
        'What is the type of the component? (m)odules, (l)ayouts, (p)rimitives:',
      )) ||
      'm',
  );
  const compType = {
    m: 'modules',
    p: 'primitives',
    c: 'core',
    l: 'layouts',
  }[compTypeRaw[0].toLowerCase()];
  const smartComponent = cliFlags['y']
    ? true
    : cliFlags['container'] ||
      (await makey.askYN('Create a (smart) container for the Comp?'));
  const ComponentName = makey.toUpperCaseFirst(componentName);
  const propsFilled = makey.templateReplace(
    smartComponent ? smartPropsBody : dumbPropsBody,
    { ComponentName },
  );
  const styleFilled = makey.templateReplace(styleBody, {});
  const containerFilled = makey.templateReplace(containerBody, {
    ComponentName,
  });
  const viewFilled = makey.templateReplace(
    smartComponent ? smartViewBody : dumbViewBody,
    { ComponentName },
  );
  const indexFilled = makey.templateReplace(
    smartComponent ? smartIndexBody : dumbIndexBody,
    { ComponentName },
  );
  makey.createFile(
    `./src/components/${compType}/${ComponentName}/${ComponentName}.props.ts`,
    propsFilled,
  );
  makey.createFile(
    `./src/components/${compType}/${ComponentName}/${ComponentName}.style.ts`,
    styleFilled,
  );
  if (smartComponent) {
    makey.createFile(
      `./src/components/${compType}/${ComponentName}/${ComponentName}.container.tsx`,
      containerFilled,
    );
  }
  makey.createFile(
    `./src/components/${compType}/${ComponentName}/${ComponentName}.view.tsx`,
    viewFilled,
  );
  makey.createFile(
    `./src/components/${compType}/${ComponentName}/index.tsx`,
    indexFilled,
  );
};
