const containerBody = `import React from 'react';
import { {{RouteName}}Props } from './{{RouteName}}.props';
import {{RouteName}}View from './{{RouteName}}.view';

const {{RouteName}}Container = (props: {{RouteName}}Props): JSX.Element => {
  return <{{RouteName}}View />;
};

export default {{RouteName}}Container;
`;

const styleBody = `import styled from '@emotion/styled';

export const Container = styled.div\`
  flex: 1;
  padding: 20px;
\`;
`;

const propsBody = `import { RouteComponentProps } from 'react-router-dom';
type Params = {};

export type {{RouteName}}Props = RouteComponentProps<Params>;

export type {{RouteName}}GeneratedProps = {};

`;

const viewBody = `import React from 'react';
import { Container } from './{{RouteName}}.style';
import { {{RouteName}}GeneratedProps } from './{{RouteName}}.props';

const {{RouteName}}View = (
  props: {{RouteName}}GeneratedProps
): JSX.Element => (
  <Container>{{RouteName}}</Container>
);

export default {{RouteName}}View;
`;

const indexBody = `export { default } from './{{RouteName}}.container';
`;

const help = `Adds a new React 'route' component.

This will create the new route folder (/src/routes/MyrouteName) and the routes files.

\`npm run domakey route MyRouteName\`

Options:
 --y            Accept all default suggestions
`;

module.exports = async ({ cliArgs, cliFlags, templateName, makey }) => {
  if (cliFlags['h'] || !cliArgs[0]) {
    makey.print(help);
    return;
  }

  const routeName = makey.toLowerCaseFirst(
    cliArgs[0] || (await makey.ask('Name of your route:')),
  );
  if (!routeName) throw Error('Please provide a route name');
  const path = makey.toLowerCaseFirst(
    cliArgs[1] || (await makey.ask('Specify route path (/):')),
  );
  if (!routeName) throw Error('Please provide a route path');
  const ROUTE_NAME = cliFlags['y']
    ? makey.camelToSnakeCaps(routeName)
    : (await makey.ask(
        `Identifier for your route: (${makey.camelToSnakeCaps(routeName)})`,
      )) || makey.camelToSnakeCaps(routeName);
  const RouteName = makey.toUpperCaseFirst(routeName);
  const propsFilled = makey.templateReplace(propsBody, { RouteName });
  const styleFilled = makey.templateReplace(styleBody, {});
  const containerFilled = makey.templateReplace(containerBody, { RouteName });
  const viewFilled = makey.templateReplace(viewBody, { RouteName });
  const indexFilled = makey.templateReplace(indexBody, { RouteName });
  makey.createFile(
    `./src/routes/${RouteName}/${RouteName}.props.ts`,
    propsFilled,
  );
  makey.createFile(
    `./src/routes/${RouteName}/${RouteName}.style.ts`,
    styleFilled,
  );
  makey.createFile(
    `./src/routes/${RouteName}/${RouteName}.container.tsx`,
    containerFilled,
  );
  makey.createFile(
    `./src/routes/${RouteName}/${RouteName}.view.tsx`,
    viewFilled,
  );
  makey.createFile(`./src/routes/${RouteName}/index.tsx`, indexFilled);

  await makey.editFile(`src/routes/index.tsx`, (existingRouteFile) =>
    existingRouteFile.replace(
      `// ROUTE IMPORT CODE GENERATOR INDICATOR DO NOT DELETE`,
      makey.templateReplace(
        `import {{RouteName}} from 'routes/{{RouteName}}';\n// ROUTE IMPORT CODE GENERATOR INDICATOR DO NOT DELETE`,
        {
          ROUTE_NAME,
          RouteName,
          path,
        },
      ),
    ),
  );

  await makey.editFile(`src/routes/index.tsx`, (existingRouteFile) =>
    existingRouteFile.replace(
      `// ROUTE ENTRY CODE GENERATOR INDICATOR DO NOT DELETE`,
      makey.templateReplace(
        `{{ROUTE_NAME}}: {\n    component: {{RouteName}},\n    path: '/{{path}}',\n  },\n  // ROUTE ENTRY CODE GENERATOR INDICATOR DO NOT DELETE`,
        {
          ROUTE_NAME,
          RouteName,
          path,
        },
      ),
    ),
  );
};
