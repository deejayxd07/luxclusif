import generateRandomColor from './generateRandomColor';

describe('generateRandomColor', () => {
  it('should generate a color in hex', () => {
    expect(generateRandomColor().length).toEqual(7);
  });
  it('should generate random colors', () => {
    const color1 = generateRandomColor();
    const color2 = generateRandomColor();

    expect(color1).not.toEqual(color2);
  });
});
