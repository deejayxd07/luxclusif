import generateUniqueId from './generateUniqueId';
import generateRandomColor from './generateRandomColor';

const generateDummyProducts = (count: number) =>
  Array.apply(null, Array(count)).map((_, index) => ({
    id: generateUniqueId(),
    name: `Key Pouch ${index + 1}`,
    descriptions:
      'The Key Pouch in iconic Monogram canvas is a playful yet practical accessory that can carry coins, cards, folded bills and other small items, in addition to keys. Secured with an LV-engraved zip, it can be hooked onto the D-ring inside most Louis Vuitton bags, or used as a bag or belt charm.on`',
    identifiers: {
      upc: `UPC${generateUniqueId()}`,
      mpn: `MPN${generateUniqueId()}`,
      brand: 'Louis Vuitton',
      gtin: `GTIN${generateUniqueId()}`,
    },
    photos: [
      'http://us.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-key-pouch-monogram-key-holders-and-bag-charms--M62650_PM2_Front%20view.png?wid=1240&hei=1240',
    ],
    color: generateRandomColor(),
    material: 'Monogram',
    categories: ['bag'],
  }));

export default generateDummyProducts;
