import generateDummyProducts from './generateDummyProducts';

describe('generateDummyProducts', () => {
  it('should generate dummy products based on the count provided', () => {
    const tenProducts = generateDummyProducts(10);
    expect(tenProducts.length).toEqual(10);

    const twentyProducts = generateDummyProducts(20);
    expect(twentyProducts.length).toEqual(20);
  });

  it('should contain all necessary product properties', () => {
    const dummyProduct = generateDummyProducts(1);
    expect(dummyProduct[0]).toHaveProperty('id');
    expect(dummyProduct[0]).toHaveProperty('name');
    expect(dummyProduct[0]).toHaveProperty('descriptions');
    expect(dummyProduct[0]).toHaveProperty('identifiers');
    expect(dummyProduct[0]).toHaveProperty('photos');
    expect(dummyProduct[0]).toHaveProperty('color');
    expect(dummyProduct[0]).toHaveProperty('material');
    expect(dummyProduct[0]).toHaveProperty('categories');
  });
});
