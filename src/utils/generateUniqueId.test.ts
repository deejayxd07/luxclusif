import generateUniqueId from './generateUniqueId';

describe('generateUniqueId', () => {
  it('should generate a unique id', () => {
    const id1 = generateUniqueId();
    const id2 = generateUniqueId();

    expect(id1).not.toEqual(id2);
  });
});
