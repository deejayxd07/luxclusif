export default function generateUniqueId(): number {
  return Math.floor(Math.random() * Date.now());
}
