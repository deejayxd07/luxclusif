import React from 'react';
import Login from './Login.container';
import renderer from 'react-test-renderer';
import { render, screen } from '@testing-library/react';

/* Ideally, we should move this on the setup file for large apps */
jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: (str: string) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('Login', () => {
  test('should contain email and password fields', () => {
    // @ts-ignore
    render(<Login onLogin={() => null} />);

    const email = screen.getByLabelText(/email/i);
    const password = screen.getByLabelText(/password/i);

    expect(email).toBeInTheDocument();
    expect(password).toBeInTheDocument();
  });
  test('should see the title', () => {
    // @ts-ignore
    render(<Login onLogin={() => null} />);
    const title = screen.getByText(/Luxclusif/i);
    expect(title).toBeInTheDocument();
  });

  test('should render correctly', () => {
    // @ts-ignore
    const tree = renderer.create(<Login />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
