import React, { useCallback } from 'react';

import { useCredentialStore } from 'api/stores/useCredentialStore';

import { LoginProps } from './Login.props';
import LoginView from './Login.view';
import { useToast } from '@chakra-ui/toast';

const LoginContainer = (props: LoginProps): JSX.Element => {
  const toast = useToast();
  const login = useCredentialStore((state) => state.login);

  const onLogin = useCallback(
    ({ email, password }, formikHelpers) => {
      const error = login(email, password);

      /* Simulating API request loading */
      setTimeout(() => {
        formikHelpers.setSubmitting(false);
        if (Boolean(error)) {
          toast({
            status: 'error',
            title: 'Error',
            description: error!,
            isClosable: true,
            duration: 3000,
          });
        }
      }, 1000);
    },
    [login],
  );

  return <LoginView onLogin={onLogin} />;
};

export default LoginContainer;
