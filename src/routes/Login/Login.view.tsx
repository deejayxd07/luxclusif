import React from 'react';

import { Box, Button, Heading, Stack } from '@chakra-ui/react';
import FormikInput from 'components/common/FormikInput';
import { Form, Formik } from 'formik';

import { LoginGeneratedProps } from './Login.props';
import { Container } from './Login.style';
import { LoginSchema } from './Login.utils';
import { useTranslation } from 'react-i18next';

const LoginView = ({ onLogin }: LoginGeneratedProps): JSX.Element => {
  const { t } = useTranslation();
  return (
    <Container>
      <Box width="100%" maxWidth={500} marginX="auto">
        <Stack
          bg="white"
          spacing={8}
          shadow="md"
          borderWidth="1px"
          p={10}
          borderRadius={5}>
          <Heading textColor="primary.500">Luxclusif</Heading>
          <Formik
            initialValues={{
              email: '',
              password: '',
            }}
            onSubmit={onLogin}
            validationSchema={LoginSchema}>
            {({ isSubmitting }) => {
              return (
                <Form>
                  <FormikInput name="email" label={t('login.email')} />
                  <FormikInput
                    name="password"
                    type="password"
                    label={t('login.password')}
                  />
                  <Button
                    type="submit"
                    width="100%"
                    mt={10}
                    isLoading={isSubmitting}>
                    {t('login.submit')}
                  </Button>
                </Form>
              );
            }}
          </Formik>
        </Stack>
      </Box>
    </Container>
  );
};

export default LoginView;
