import { FormikHelpers, FormikValues } from 'formik';
import { RouteComponentProps } from 'react-router-dom';
type Params = {};

export type LoginProps = RouteComponentProps<Params>;

type LoginValues = {
  email: string;
  password: string;
};
export type LoginGeneratedProps = {
  onLogin: (
    values: LoginValues,
    formikHelpers: FormikHelpers<LoginValues>,
  ) => void;
};
