import React from 'react';

import AuthGuard from 'components/core/AuthGuard';
import RouteWithSubRoutes from 'components/core/RouteWithSubRoutes';
import { Helmet } from 'react-helmet';
import { Switch, useLocation } from 'react-router-dom';
import Login from 'routes/Login';
import ProductList from 'routes/ProductList';
import { Flex } from '@chakra-ui/layout';
import Product from './Product';

export const ROUTES = {
  LOGIN: {
    component: Login,
    path: '/login',
  },
  PRODUCT_LIST: {
    component: ProductList,
    path: '/product-list',
  },
  PRODUCT_CREATE: {
    component: Product,
    path: '/product/create',
  },
  PRODUCT_EDIT: {
    component: Product,
    path: '/product/edit/:id',
  },
};

const Routes = (): JSX.Element => {
  const location = useLocation();
  const { pathname } = location;

  const auth = pathname === ROUTES.LOGIN.path;

  const notFound =
    Object.values(ROUTES)
      .map((r) => r.path)
      .filter((p) => pathname.includes(p)).length === 0;

  return (
    <>
      {auth && (
        <Helmet>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Helmet>
      )}
      <Flex flex={1} bg="#f1f1f1">
        <Flex
          flexGrow={1}
          flexBasis={0}
          maxWidth="1200px"
          justifyContent="center"
          marginX="auto">
          <AuthGuard auth={auth} notFound={notFound}>
            <Switch>
              {Object.values(ROUTES).map((route) => (
                <RouteWithSubRoutes key={route.path} {...route} exact />
              ))}
            </Switch>
          </AuthGuard>
        </Flex>
      </Flex>
    </>
  );
};

export default Routes;
