import { useToast } from '@chakra-ui/react';
import { Product, useProductStore } from 'api/stores/useProductStore';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import { ROUTES } from 'routes';
import { ProductProps } from './Product.props';
import ProductView from './Product.view';

const ProductContainer = (props: ProductProps): JSX.Element => {
  const {
    addProduct,
    getProductById,
    selectedProduct,
    updateProduct,
    clearSelectedProduct,
  } = useProductStore((state) => state);
  const toast = useToast();
  const { push } = useHistory();
  const { id } = useParams<{ id?: string | undefined }>();

  useEffect(() => {
    if (id) {
      getProductById(Number(id));
    }

    return clearSelectedProduct;
  }, []);

  const onSuccess = (description: string) => {
    toast({
      status: 'success',
      title: 'Success',
      description,
      isClosable: true,
      duration: 1000,
    });
    push(ROUTES.PRODUCT_LIST.path);
  };

  /* The Product component serves as the same form for create and update actions,
   we determine what action to take depending on the `id` params,
   which is only available on the /product/edit/:id route */

  const onSubmit = (values: Product) => {
    if (id) {
      updateProduct(values, () => {
        onSuccess('Successfully updated product!');
      });
    } else {
      addProduct(values, () => {
        onSuccess('Successfully created product!');
      });
    }
  };

  return <ProductView onSubmit={onSubmit} selectedProduct={selectedProduct} />;
};

export default ProductContainer;
