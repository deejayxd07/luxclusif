import { Product } from 'api/stores/useProductStore';
import { RouteComponentProps } from 'react-router-dom';
type Params = {};

export type ProductProps = RouteComponentProps<Params>;

export type ProductGeneratedProps = {
  onSubmit: (product: Product) => void;
  selectedProduct: Product | undefined;
};
