import * as Yup from 'yup';

export const urlRegex = /(https?:\/\/.*\.(?:png|jpg))/;

export const ProductSchema = Yup.object().shape({
  name: Yup.string().required('Required'),
  descriptions: Yup.string().required('Required'),
  upc: Yup.string(),
  mpn: Yup.string(),
  brand: Yup.string().required('Required'),
  gtin: Yup.string(),
  photos: Yup.array()
    .transform(function (value, originalValue) {
      if (this.isType(value) && value !== null) {
        return value;
      }
      return originalValue ? originalValue.split(/[\s,]+/) : [];
    })
    .of(Yup.string().matches(urlRegex, 'Invalid Photo URL ').required()),
  categories: Yup.string().required('Required'),
  color: Yup.string().required('Required'),
  material: Yup.string().required('Required'),
});
