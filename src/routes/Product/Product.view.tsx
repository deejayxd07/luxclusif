import React from 'react';
import { Container } from './Product.style';
import { ProductGeneratedProps } from './Product.props';
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormHelperText,
  Heading,
  Stack,
  Text,
  Image,
} from '@chakra-ui/react';
import { Form, Formik } from 'formik';
import { ProductSchema } from './Product.utils';
import FormikInput from 'components/common/FormikInput';
import { Product } from 'api/stores/useProductStore';
import { ChevronLeftIcon } from '@chakra-ui/icons';
import { useHistory } from 'react-router';
import { useTranslation } from 'react-i18next';

const DEFAULT_PRODUCT: Product = {
  name: '',
  descriptions: '',
  identifiers: {
    upc: '',
    mpn: '',
    brand: '',
    gtin: '',
  },
  photos: [],
  categories: [],
  color: '',
  material: '',
};

const ProductView = ({
  selectedProduct = DEFAULT_PRODUCT,
  onSubmit,
}: ProductGeneratedProps): JSX.Element => {
  const {
    name,
    descriptions,
    identifiers,
    photos,
    color,
    material,
    categories,
  } = selectedProduct;
  const { t } = useTranslation();

  const { upc, mpn, brand, gtin } = identifiers;

  const { goBack } = useHistory();

  return (
    <Container>
      <Box width="100%" maxWidth={700} marginX="auto">
        <Stack
          bg="white"
          spacing={8}
          shadow="md"
          borderWidth="1px"
          p={10}
          borderRadius={5}>
          <Flex justify="space-between" alignItems="center">
            <Heading textColor="primary.500">
              {selectedProduct.id
                ? t('product.titles.edit')
                : t('product.titles.create')}{' '}
              Product
            </Heading>
            <Flex onClick={goBack} alignItems="center" cursor="pointer">
              <ChevronLeftIcon w={10} h={10} />
              <Text fontSize="xl">{t('product.helperTexts.back')}</Text>
            </Flex>
          </Flex>
          <Formik
            enableReinitialize
            initialValues={{
              name,
              descriptions,
              upc,
              mpn,
              brand,
              gtin,
              photos: photos ? photos.toString() : '',
              categories: categories ? categories.toString() : '',
              color,
              material,
            }}
            onSubmit={(values) => {
              const {
                name,
                descriptions,
                upc,
                mpn,
                brand,
                gtin,
                photos,
                categories,
                color,
                material,
              } = values;

              onSubmit({
                name,
                descriptions,
                identifiers: {
                  upc,
                  mpn,
                  brand,
                  gtin,
                },
                photos: photos ? photos.split(',') : [],
                categories: categories ? categories.split(',') : [],
                color,
                material,
              });
            }}
            validationSchema={ProductSchema}>
            {({ isSubmitting, values }) => {
              const photos = values.photos ? values.photos.split(',') : [];

              return (
                <Form>
                  <FormikInput name="name" label={t('product.name')} />
                  <FormikInput
                    name="descriptions"
                    label={t('product.descriptions')}
                  />
                  <FormikInput
                    name="upc"
                    label={t('product.identifiers.upc')}
                  />
                  <FormikInput
                    name="mpn"
                    label={t('product.identifiers.mpn')}
                  />
                  <FormikInput
                    name="brand"
                    label={t('product.identifiers.brand')}
                  />
                  <FormikInput
                    name="gtin"
                    label={t('product.identifiers.gtin')}
                  />
                  <FormControl>
                    <FormikInput name="photos" label={t('product.photos')} />
                    <FormHelperText>
                      {t('product.helperTexts.photos')}
                    </FormHelperText>
                  </FormControl>

                  <Stack
                    direction="row"
                    flex={1}
                    spacing={4}
                    flexWrap="wrap"
                    justify="center">
                    {photos.length &&
                      photos.map((photoUrl: string, index: number) => (
                        <Image
                          key={photoUrl + index}
                          h={250}
                          w={250}
                          objectFit="cover"
                          borderRadius="5px"
                          src={photoUrl}
                        />
                      ))}
                  </Stack>

                  <FormikInput
                    name="categories"
                    label={t('product.categories')}
                  />

                  <FormikInput name="color" label={t('product.color')} />
                  <FormikInput name="material" label={t('product.material')} />
                  <Button
                    type="submit"
                    width="100%"
                    mt={10}
                    isLoading={isSubmitting}>
                    {t('product.submit')}
                  </Button>
                </Form>
              );
            }}
          </Formik>
        </Stack>
      </Box>
    </Container>
  );
};

export default ProductView;
