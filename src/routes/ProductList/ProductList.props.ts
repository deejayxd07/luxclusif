import { Product } from 'api/stores/useProductStore';
import { RouteComponentProps } from 'react-router-dom';
type Params = {};

export type ProductListProps = RouteComponentProps<Params>;

export type ProductListGeneratedProps = {
  products: Array<Product> | undefined;
  onProductClick: (id: number) => void;
  onAddClick: () => void;
  onSearch: (searchTerm: string) => void;
};
