import { useProductStore } from 'api/stores/useProductStore';
import React, { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { ProductListProps } from './ProductList.props';
import ProductListView from './ProductList.view';

const ProductListContainer = (props: ProductListProps): JSX.Element => {
  const { searchResults, searchProducts } = useProductStore((state) => state);
  const { push } = useHistory();

  useEffect(() => {
    onSearch('');
  }, []);

  const onProductClick = useCallback(
    (id: number) => {
      push(`/product/edit/${id}`, { id });
    },
    [push],
  );

  const onAddClick = useCallback(() => {
    push('/product/create');
  }, [push]);

  /* For performance issues, search will only be invoked after submit*/
  const onSearch = (searchTerm: string) => {
    searchProducts(searchTerm);
  };

  return (
    <ProductListView
      products={searchResults}
      onProductClick={onProductClick}
      onAddClick={onAddClick}
      onSearch={onSearch}
    />
  );
};

export default ProductListContainer;
