import React from 'react';
import { Container } from './ProductList.style';
import { ProductListGeneratedProps } from './ProductList.props';
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableCaption,
  Box,
  Heading,
  Stack,
  Flex,
  Text,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Select,
  IconButton,
  Tooltip,
  InputRightElement,
  InputGroup,
  FormHelperText,
  FormControl,
} from '@chakra-ui/react';
import {
  ArrowRightIcon,
  ArrowLeftIcon,
  ChevronRightIcon,
  ChevronLeftIcon,
  CloseIcon,
  SearchIcon,
} from '@chakra-ui/icons';
import { AddIcon } from '@chakra-ui/icons';
// @ts-ignore
import { useTable, usePagination } from 'react-table';
import { Product } from 'api/stores/useProductStore';
import { Formik, Form } from 'formik';
import FormikInput from 'components/common/FormikInput';
import { SearchSchema } from './ProductList.utils';
import { useTranslation } from 'react-i18next';

const perPage = [5, 10, 15, 20];

const ProductPagination = ({
  columns,
  data,
  onProductClick,
}: {
  columns: Array<Object>;
  data: Array<Product | undefined>;
  onProductClick: (id: number) => void;
}) => {
  const {
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
    },
    usePagination,
  );
  const { t } = useTranslation();

  if (!data) {
    return <Box />;
  }

  return (
    <>
      <Table variant="simple">
        {!page && (
          <TableCaption>{t('product.helperTexts.noProducts')}</TableCaption>
        )}
        <Thead>
          <Tr>
            <Th>{t('product.name')}</Th>
            <Th>{t('product.descriptions')}</Th>
            <Th>{t('product.identifiers.brand')}</Th>
            <Th>{t('product.color')}</Th>
            <Th>{t('product.material')}</Th>
          </Tr>
        </Thead>
        <Tbody>
          {page &&
            page.map(({ original }: { original: Product }) => {
              const {
                id,
                name,
                descriptions,
                color,
                material,
                identifiers,
              } = original;
              return (
                <Tr key={id} cursor="pointer">
                  <Td onClick={() => onProductClick(id!)}>{name}</Td>
                  <Td onClick={() => onProductClick(id!)}>
                    <Text noOfLines={2}>{descriptions}</Text>
                  </Td>
                  <Td onClick={() => onProductClick(id!)}>
                    {identifiers.brand}
                  </Td>
                  <Td onClick={() => onProductClick(id!)}>
                    <Box padding={4} bg={color} />
                  </Td>
                  <Td onClick={() => onProductClick(id!)}>{material}</Td>
                </Tr>
              );
            })}
        </Tbody>
      </Table>
      <Flex justifyContent="space-between" m={4} alignItems="center">
        <Flex>
          <Tooltip label="First Page">
            <IconButton
              aria-label="First Page"
              onClick={() => gotoPage(0)}
              isDisabled={!canPreviousPage}
              icon={<ArrowLeftIcon h={3} w={3} />}
              mr={4}
            />
          </Tooltip>
          <Tooltip label={t('product.helperTexts.pagination.previousPage')}>
            <IconButton
              aria-label={t('product.helperTexts.pagination.previousPage')}
              onClick={previousPage}
              isDisabled={!canPreviousPage}
              icon={<ChevronLeftIcon h={6} w={6} />}
            />
          </Tooltip>
        </Flex>
        <Flex alignItems="center">
          <Text mr={8}>
            {t('product.helperTexts.pagination.page')}{' '}
            <Text fontWeight="bold" as="span">
              {pageIndex + 1}
            </Text>{' '}
            {t('product.helperTexts.pagination.of')}{' '}
            <Text fontWeight="bold" as="span">
              {pageOptions.length}
            </Text>
          </Text>
          <Text>{t('product.helperTexts.pagination.goToPage')}</Text>{' '}
          <NumberInput
            ml={2}
            mr={8}
            w={28}
            min={1}
            max={pageOptions.length}
            onChange={(_, valueAsNumber) => {
              const page = valueAsNumber ? valueAsNumber - 1 : 0;
              gotoPage(page);
            }}
            defaultValue={pageIndex + 1}>
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
          <Select
            w={32}
            value={pageSize}
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}>
            {perPage.map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                {t('product.helperTexts.pagination.show')} {pageSize}
              </option>
            ))}
          </Select>
        </Flex>
        <Flex>
          <Tooltip label={t('product.helperTexts.pagination.nextPage')}>
            <IconButton
              aria-label={t('product.helperTexts.pagination.nextPage')}
              onClick={nextPage}
              isDisabled={!canNextPage}
              icon={<ChevronRightIcon h={6} w={6} />}
            />
          </Tooltip>
          <Tooltip label={t('product.helperTexts.pagination.lastPage')}>
            <IconButton
              aria-label={t('product.helperTexts.pagination.lastPage')}
              onClick={() => gotoPage(pageCount - 1)}
              isDisabled={!canNextPage}
              icon={<ArrowRightIcon h={3} w={3} />}
              ml={4}
            />
          </Tooltip>
        </Flex>
      </Flex>
    </>
  );
};

const ProductListView = ({
  products,
  onProductClick,
  onAddClick,
  onSearch,
}: ProductListGeneratedProps): JSX.Element => {
  const { t } = useTranslation();
  const columns = React.useMemo(
    () => [
      {
        Header: 'Name',
        columns: [
          {
            Header: 'Name',
            accessor: 'name',
          },
          {
            Header: 'Description',
            accessor: 'description',
          },
          {
            Header: 'Brand',
            accessor: 'brand',
          },
          {
            Header: 'Color',
            accessor: 'color',
          },
          {
            Header: 'Material',
            accessor: 'material',
          },
        ],
      },
    ],
    [],
  );
  return (
    <Container>
      <Box width="100%" maxWidth={800} marginX="auto">
        <Stack
          bg="white"
          spacing={8}
          shadow="md"
          borderWidth="1px"
          p={10}
          borderRadius={5}>
          <Flex justify="space-between" alignItems="center">
            <Heading textColor="primary.500">
              {t('product.titles.list')}
            </Heading>
            <AddIcon onClick={onAddClick} cursor="pointer" w={6} h={6} />
          </Flex>
          <Text fontSize="sm">{t('product.helperTexts.info')}</Text>

          <Formik
            initialValues={{
              searchTerm: '',
            }}
            onSubmit={({ searchTerm }) => {
              onSearch(searchTerm);
            }}
            validationSchema={SearchSchema}>
            {({ values, setValues }) => {
              return (
                <Form>
                  <Flex alignItems="center">
                    <FormControl>
                      <InputGroup margin={0}>
                        <FormikInput
                          placeholder={t('product.search')}
                          name="searchTerm"
                        />
                        {values.searchTerm && (
                          <InputRightElement
                            marginTop="15px"
                            children={<CloseIcon />}
                            onClick={() => setValues({ searchTerm: '' })}
                          />
                        )}
                      </InputGroup>
                      <FormHelperText>
                        {t('product.helperTexts.search')}
                      </FormHelperText>
                    </FormControl>

                    <IconButton
                      marginBottom="10px"
                      type="submit"
                      aria-label="Search"
                      icon={<SearchIcon />}
                    />
                  </Flex>
                </Form>
              );
            }}
          </Formik>

          <ProductPagination
            columns={columns}
            data={products ? products : []}
            onProductClick={onProductClick}
          />
        </Stack>
      </Box>
    </Container>
  );
};

export default ProductListView;
