import * as Yup from 'yup';

export const SearchSchema = Yup.object().shape({
  searchTerm: Yup.string(),
});
