import styled from '@emotion/styled';

export const Container = styled.div`
  flex: 1;
  padding: 20px;
`;
