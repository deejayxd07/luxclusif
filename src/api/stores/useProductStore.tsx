import { Immutable } from 'immer';
import generateDummyProducts from 'utils/generateDummyProducts';
import generateUniqueId from 'utils/generateUniqueId';
import create, { State } from 'zustand';
import { persist } from 'zustand/middleware';
import { immer } from './middlewares';
import Fuse from 'fuse.js';

interface ProductIdentifiers {
  upc: string;
  mpn: string;
  brand: string;
  gtin: string;
}

export type Product = {
  id?: number;
  name: string;
  descriptions: string;
  identifiers: ProductIdentifiers;
  photos: Array<string>;
  categories: Array<string>;
  color: string;
  material: string;
};

export type ImmutableProduct = Immutable<Product>;

export interface ProductStore extends State {
  products: Array<Product>;
  searchResults: Array<Product>;
  selectedProduct: Product | undefined;
  addProduct: (product: Product, callback: () => void) => void;
  updateProduct: (updatedProductValues: Product, callback: () => void) => void;
  getProductById: (id: number) => void;
  clearSelectedProduct: () => void;
  searchProducts: (value: string) => void;
}

/* We're using an immer middleware for handling the immutable state
  Values are persisted on the local storage thru zustand persist
*/
const storeWithImmer = immer<ProductStore, ProductStore>((set, get) => ({
  products: generateDummyProducts(20),
  selectedProduct: undefined,
  searchResults: [],
  addProduct: (product: Product, callback: () => void) => {
    const newProductWithId: Product = {
      id: generateUniqueId(),
      ...product,
    };

    set((state) => {
      state.products.unshift(newProductWithId);
    });

    callback();
  },
  updateProduct: (updatedProductValues: Product, callback: () => void) => {
    const id = get().selectedProduct?.id || '';

    if (id) {
      set((state) => {
        const index = state.products?.findIndex((product) => product.id === id);
        if (index !== -1) {
          state.products[index] = { id, ...updatedProductValues };
        }
      });

      callback();
    }
  },
  getProductById: (id: number) => {
    const products = get().products || [];
    const product: Array<Product> = products.filter(
      (product: Product) => product.id === id,
    );

    if (product && product.length) {
      set((state) => {
        state.selectedProduct = product[0];
      });
    }
  },
  clearSelectedProduct: () => {
    set((state) => {
      state.selectedProduct = undefined;
    });
  },
  searchProducts: (value: string) => {
    if (value) {
      const fuse = new Fuse(get().products || [], {
        keys: ['name', ' description', 'brand', 'color', 'material'],
        threshold: 0.7,
      });
      const search = fuse.search(value);
      const searchResults = search.map((result) => result.item);

      set((state) => {
        state.searchResults = searchResults;
      });
    } else {
      set((state) => {
        state.searchResults = get().products;
      });
    }
  },
}));

const useProductStore = create(
  persist(storeWithImmer, {
    name: 'product-storage',
  }),
);

export { useProductStore };
