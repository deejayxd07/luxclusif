import produce from 'immer';
import { GetState, SetState, State, StoreApi } from 'zustand';

type StateCreator<
  T extends State,
  CustomSetState = SetState<T>,
  U extends State = T
> = (set: CustomSetState, get: GetState<T>, api: StoreApi<T>) => U;

export const immer = <T extends State, U extends State>(
  config: StateCreator<T, (fn: (draft: T) => void) => void, U>,
): StateCreator<T, SetState<T>, U> => (set, get, api) =>
  config((fn: any) => set(produce(fn) as (state: T) => T), get, api);
