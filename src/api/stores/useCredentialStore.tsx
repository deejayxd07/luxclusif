import { Immutable } from 'immer';
import create, { State } from 'zustand';
import { persist } from 'zustand/middleware';
import { immer } from './middlewares';

type User = Immutable<{
  email: string;
  password: string;
}>;

export interface CredentialStore extends State {
  currentUser?: User | undefined;
  token?: string | null;
  login: (email: string, password: string) => string | undefined;
  logout: () => void;
}

/* For the purposes of this exam, I've hardcoded the email and password for the authentication*/
const MockCredentials = {
  email: 'pleasehiredavid@luxclusif.com',
  password: 'ShouldWe?PleaseDo:)',
};

/* We're using an immer middleware for handling the immutable state */
const storeWithImmer = immer((set) => ({
  currentUser: {
    email: '',
    password: '',
  },
  token: undefined,
  login: (email: string, password: string) => {
    const madeUpToken = 'ThisIsAMadeUpToken!@&^@&*!^@&*!^';

    /* On real world scenarios, the user data is returned on success and an error response when there's an error.
    For the purposes of this exam, I have made it return a message only when there's an error.
    Since we're already setting the new values here, we don't need to return it
    */

    if (email && password) {
      if (
        email === MockCredentials.email &&
        password === MockCredentials.password
      ) {
        const newValues = {
          currentUser: {
            email,
            password,
          },
          token: madeUpToken,
        };

        set(() => newValues);
      } else {
        return 'Invalid username and password';
      }
    }
  },
  logout: async () => {
    /* On hindsight, this seems that we're mutating the currentUser and token values, but we're not since we're using the immer middleware, this returns a new value
    More on immer here: https://immerjs.github.io/immer/*/
    set((state) => {
      state.currentUser = undefined;
      state.token = undefined;
    });
  },
}));

const useCredentialStore = create<CredentialStore>(
  // @ts-ignore
  persist(storeWithImmer, {
    name: 'auth-storage',
  }),
);

export { useCredentialStore };
