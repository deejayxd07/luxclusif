import React, { useEffect, useState } from 'react';

import { Box, Spinner } from '@chakra-ui/react';
import { useCredentialStore } from 'api/stores/useCredentialStore';
import { AuthGuardPublicProps } from 'components/core/AuthGuard/AuthGuard.props';
import { useHistory, useLocation } from 'react-router-dom';
import { ROUTES } from 'routes';

const AuthGuard = ({
  children,
  notFound,
  auth,
}: AuthGuardPublicProps): JSX.Element => {
  const history = useHistory();
  const { pathname } = useLocation();
  const [redirect, setRedirecting] = useState(false);
  const [isKeyFetched, setIsKeyFetched] = useState(false);

  const sessionRedirectRoutes = ['/', ROUTES.LOGIN.path];
  const token = useCredentialStore((state) => state.token);

  const key = token || '';

  useEffect(() => setIsKeyFetched(true), [token]);

  useEffect(() => {
    const checkRedirect = (key?: string) => {
      if (key) {
        if (sessionRedirectRoutes.includes(pathname)) {
          history.replace(ROUTES.PRODUCT_LIST.path);
        }
      } else if (notFound) {
        setRedirecting(true);

        setTimeout(() => {
          setRedirecting(false);
          if (key) {
            history.replace(ROUTES.PRODUCT_LIST.path);
          } else if (!key) {
            history.replace(ROUTES.LOGIN.path);
          }
        }, 500);
      } else {
        history.replace(ROUTES.LOGIN.path);
      }
    };

    if (isKeyFetched) {
      checkRedirect(key);
    }
  }, [key, auth, notFound, history, isKeyFetched]);

  return (
    <>
      {redirect ? (
        <Box
          display="flex"
          height="100vh"
          width="100%"
          alignItems="center"
          justifyContent="center">
          <Spinner size="xl" />
        </Box>
      ) : (
        <>{children}</>
      )}
    </>
  );
};

export default React.memo(AuthGuard);
