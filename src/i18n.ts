import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import languageDetector from 'i18next-browser-languagedetector';

const resources = {
  en: {
    translation: {
      login: {
        email: 'EMAIL',
        password: 'PASSWORD',
        submit: 'Submit',
      },
      product: {
        titles: {
          edit: 'Edit Product',
          create: 'Create Product',
          list: 'Products',
        },
        name: 'NAME',
        descriptions: 'DESCRIPTIONS',
        identifiers: {
          upc: 'UPC',
          mpn: 'MPN',
          brand: 'BRAND',
          gtin: 'GTIN',
        },
        photos: 'PHOTOS (comma separated links)',
        categories: 'CATEGORIES (comma separated)',
        color: 'COLOR',
        material: 'MATERIAL',
        search: 'Search',
        submit: 'Submit',
        helperTexts: {
          photos: 'Please only add valid image urls',
          noProducts: 'No Products',
          search:
            'Search and filter by name, description, color, brand and material',
          info:
            "Hi Tester! It's David. I have pre-populated the list with mock data so you can test the pagination without needing to create multiple products. I hope you enjoy using this mini app :)",
          back: 'Back',
          pagination: {
            page: 'Page',
            of: 'of',
            goToPage: 'Go to page:',
            show: 'Show',
            nextPage: 'Next Page',
            previousPage: 'Previous Page',
            lastPage: 'Last Page',
          },
        },
      },
    },
  },
  /* I don't know french */
  fr: {
    translation: {
      login: {
        email: 'EMAIL',
        password: 'PASSWORD',
        submit: 'Submit',
      },
      product: {
        titles: {
          edit: 'Edit Product',
          create: 'Create Product',
          list: 'Products',
        },
        name: 'NAME',
        descriptions: 'DESCRIPTIONS',
        identifiers: {
          upc: 'UPC',
          mpn: 'MPN',
          brand: 'BRAND',
          gtin: 'GTIN',
        },
        photos: 'PHOTOS (comma separated links)',
        categories: 'CATEGORIES (comma separated)',
        color: 'COLOR',
        material: 'MATERIAL',
        search: 'Search',
        submit: 'Submit',
        helperTexts: {
          photos: 'Please only add valid image urls',
          noProducts: 'No Products',
          search:
            'Search and filter by name, description, color, brand and material',
          info:
            "Hi Tester! It's David. I have pre-populated the list with mock data so you can test the pagination without needing to create multiple products. I hope you enjoy using this mini app :)",
          back: 'Back',
          pagination: {
            page: 'Page',
            of: 'of',
            goToPage: 'Go to page:',
            show: 'Show',
            nextPage: 'Next Page',
            previousPage: 'Previous Page',
            lastPage: 'Last Page',
          },
        },
      },
    },
  },
};

i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: 'en',
    keySeparator: '.',
    interpolation: {
      escapeValue: false,
    },
    detection: {
      order: [
        'querystring',
        'cookie',
        'localStorage',
        'sessionStorage',
        'navigator',
        'htmlTag',
        'path',
        'subdomain',
      ],
    },
  });

export default i18n;
